#!/bin/bash

for filename in *.eep; do
   awk 'FNR == 8 {print $1,$2}' $filename > ${filename}.trim
   grep -v "#" $filename | awk '{print $1,$2,$3,$7,$12,$14,$15,$20}' >> ${filename}.trim
done
