Draine B.~T., Li A., Hensley B.~S., Hunt L.~K., Sandstrom K., Smith J.-D.~T., 2021, ApJ, 917, 3
Dataset https://doi.org/10.7910/DVN/LPUHIQ

pahspec.out_bc03_z0.02_3e6_0.00_st_std = Emission spectra for
   BC03 incident spectra
   Z=0.02 (solar metallicity)
   3e6 yr population
   log_10(U) = 0.00
   standard size distribution
   standard ionization fraction-vs-size distribution

===========================

The emission spectra provided here are from the paper "Excitation of PAH Emission: Dependence on Starlight Spectrum, Intensity, PAH Size Distribution, and PAH Ionization" (Draine, Li, Hensley, Hunt, Sandstrom, & Smith 2021, Astrophysical Journal, in press). Please cite that paper if you use these data.
Preprint available at https://arxiv.org/abs/2011.07046
We consider 14 different radiation spectra:
   mmpisrf : mMMP spectrum (see Draine, Li, Hensley et al 2021)
   bc03_z0.02_3e6 : BC03 3 Myr old stellar population with Z=0.02
   bc03_z0.0004_1e7 : BC03 10 Myr old stellar population with Z=0.0004
   bc03_z0.02_1e7 : BC03 10 Myr old stellar population with Z=0.02
   bc03_z0.02_1e8 : BC03 100 Myr old stellar population with Z=0.02
   bc03_z0.02_3e8 : BC03 300 Myr old stellar population with Z=0.02
   bc03_z0.02_1e9 : BC03 1 Gyr old stellar population with Z=0.02
   bpass_z0.02_3e6 : BPASS 3 Myr old stellar population with Z=0.02
   bpass_z0.001_1e7 : BPASS 10 Myr old stellar population with Z=0.001
   bpass_z0.02_1e7 : BPASS 10 Myr old stellar population with Z=0.02
   bpass_z0.02_1e8 : BPASS 100 Myr old stellar population with Z=0.02
   bpass_z0.02_3e8 : BPASS 300 Myr old stellar population with Z=0.02
   bpass_z0.02_1e9 : BPASS 1 Gyr old stellar population with Z=0.02
   m31blge : M31 bulge spectrum

Note:
   The mMMP ("modified Mathis, Mezger & Panagia") spectrum is based on Mathis, Mezger & Panagia (1983), with parameters modified as described in Draine (2011).
   The BC03 model spectra for single-age stellar populations are from Bruzual & Charlot (2003)
   The BPASS model spectra for single-age stellar populations with binary evolution are from Eldridge et al. (2017) and Stanway & Eldridge (2018)
   The M31 bulge spectrum uses the spectrum obtained by Groves et al. (2012) for the bulge of M31.

   The above spectra are all shown in Figure 1 of Draine, Li, Hensley et al (2021).
   Each spectrum (for intensity parameter U=1) is included in a file in the appropriate library (see below).

We calculate heating of dust grains, and resulting infrared emission, for each of the above radiation fields, for selected values of the dimensionless intensity scaling parameter U, where U=1 corresponds to an intensity such that a "standard grain" (taken to be an 1.6:1 oblate spheroid of "astrodust" with a_eff=0.10um, b/a=1.6, and porosity P=0.2) is heated at the same rate as in the modified MMP estimate for the starlight intensity in the solar neighborhood [see Draine (2011) or Draine, Li, Hensley et al (2021) for discussion of the mMMP spectrum].

The PAH properties are almost identical to the model of Draine & Li (2007), but with some small adjusts in bandstrengths for features at 14.19um, 17.04um, 17.375um, and 17.87um (see Draine, Li, Hensley et al (2021).

For the properties of "astrodust", see Draine & Hensley (2021).

We also calculate heating of dust grains, and resulting infrared emission, for dust distributed in a "slab" of total thickness A_V = 2 mag. The radiation incident on the cloud surface is assumed to be from one of the above-listed spectra, with an intensity at the cloud surface characterized by the scaling parameter U. The cloud is illuminated on one side only. The dust slab is divided into 20 zones, with the radiation attenuated and reddened as it propagates into the cloud, as described in Draine, Li, Hensley et al (2021).

Results are presented for values of lgU = log10(U) = 0.00, 0.50, 1.00, ..., 6.50, 7.00

The results for each of the different spectra are located in different libraries. We provide gzipped tarfiles containing complete results for each of the starlight models:
   mMMP.tgz
   BC03_Z0.02_3Myr.tgz
   BC03_Z0.0004_10Myr.tgz
   BC03_Z0.02_10Myr.tgz
   BC03_Z0.02_100Myr.tgz
   BC03_Z0.02_300Myr.tgz
   BC03_Z0.02_1Gyr.tgz
   BPASS_Z0.02_3Myr.tgz
   BPASS_Z0.001_10Myr.tgz
   BPASS_Z0.02_10Myr.tgz
   BPASS_Z0.02_100Myr.tgz
   BPASS_Z0.02_300Myr.tgz
   BPASS_Z0.02_1Gyr.tgz
   m31bulge.tgz

We also provide libraries for models with starlight incident on a cloud with A_V=2mag of extinction:
   mMMP_slab.tgz
   BC03_Z0.02_3Myr_slab.tgz
   BC03_Z0.0004_10Myr_slab.tgz
   BC03_Z0.02_10Myr_slab.tgz
   BC03_Z0.02_100Myr_slab.tgz
   BC03_Z0.02_300Myr_slab.tgz
   BC03_Z0.02_1Gyr_slab.tgz
   BPASS_Z0.02_3Myr_slab.tgz
   BPASS_Z0.001_10Myr_slab.tgz
   BPASS_Z0.02_10Myr_slab.tgz
   BPASS_Z0.02_100Myr_slab.tgz
   BPASS_Z0.02_300Myr_slab.tgz
   BPASS_Z0.02_1Gyr_slab.tgz
   m31bulge_slab.tgz

Each library contains files of four different types:
   Emission spectra for selected radiation fields, PAH size distributions, and PAH ionizations, e.g., the mMMP library includes
      pahspec.out_mmpisrf_0.00_st_std.gz : mMMP starlight with lgU=0.00, "standard" PAH ionization ("st"), and "standard" PAH size distribution ("std")
      pahspec.out_mmpisrf_0.00_st_sma.gz : mMMP starlight with lgU=0.00, standard PAH ionization ("st"), and "small" PAH size distribution ("sma")
      pahspec.out_mmpisrf_0.00_st_lrg.gz : mMMP starlight with lgU=0.00, standard PAH ionization ("st"), and "large" PAH size distribution ("lrg")
      pahspec.out_mmpisrf_0.00_hi_std.gz : mMMP starlight with lgU=0.00, "high" PAH ionization ("hi"), and "standard" PAH size distribution ("std")
      pahspec.out_mmpisrf_0.00_hi_sma.gz : mMMP starlight with lgU=0.00, "high" PAH ionization ("hi"), and "small" PAH size distribution ("sma")
      pahspec.out_mmpisrf_0.00_hi_lrg.gz : mMMP starlight with lgU=0.00, "high" PAH ionization ("hi"), and "large" PAH size distribution ("lrg")
      pahspec.out_mmpisrf_0.00_lo_std.gz : mMMP starlight with lgU=0.00, "low" PAH ionization ("lo"), and "standard" PAH size distribution ("std")
      pahspec.out_mmpisrf_0.00_lo_sma.gz : mMMP starlight with lgU=0.00, "low" PAH ionization ("lo"), and "small" PAH size distribution ("sma")
      pahspec.out_mmpisrf_0.00_lo_lrg.gz : mMMP starlight with lgU=0.00, "low" PAH ionization ("lo"), and "large" PAH size distribution ("lrg")

   Emission spectra for individual dust grains, heated by selected radiation fields and intensities, e.g.,
      iout_graD16emtPAHib_mmpisrf_0.00.gz : PAH^+ particles of different sizes, heated by mMMP radiation spectrum with lgU=0.00
      iout_graD16emtPAHnb_mmpisrf_0.00.gz : PAH^0 particles of different sizes, heated by mMMP radiation spectrum with lgU=0.00
      iout_DH21Ad_P0.20_0.00_mmpisrf_0.00.gz : DH21 astrodust with porosity 0.20, oblate spheroids with b/a=1.6: particles of different sizes, heated by mMMP radiation spectrum with lgU=0.00

   Starlight spectra used in the calculations, e.g.,
      isrf_mmpisrf_0.00.gz : mMMP starlight spectrum with lgU=0.00

   Grain sizes, and abundance for each size, for Astrodust, PAH+, and PAH0 used in the calculations, e.g.,
      pahspec_dnda.out_st_std : "standard" ionization and "standard" PAH size distribution
      pahspec_dnda.out_st_sma : "standard" ionization and "small" PAH size distribution
      pahspec_dnda.out_st_lrg : "standard" ionization and "large" PAH size distribution
      pahspec_dnda.out_hi_std : "high" ionization and "standard" PAH size distribution
      pahspec_dnda.out_hi_sma : "high" ionization and "small" PAH size distribution
      pahspec_dnda.out_hi_lrg : "high" ionization and "large" PAH size distribution
      pahspec_dnda.out_lo_std : "low" ionization and "standard" PAH size distribution
      pahspec_dnda.out_lo_sma : "low" ionization and "small" PAH size distribution
      pahspec_dnda.out_lo_lrg : "low" ionization and "large" PAH size distribution

References:
   Bruzual, G, & Charlot, S. 2003, MNRAS, 344, 1000
   Draine, B.T. 2011, Physics of the Interstellar and Intergalactic Medium (Princeton: Princeton University Press)
   Draine, B.T., & Hensley, B.S. 2021, ApJ, 909:94
   Draine, B.T., & Li, A. 2007, ApJ, 657, 810
   Draine, B.T., Li, A., Hensley, B.S., Hunt, L.K., Sandstrom, K., and Smith, J.-D.T 2021, ApJ, in press, https://ui.adsabs.harvard.edu/abs/2020arXiv201107046D/abstract
   Eldridge, J.J., Stanway, E.R., Xiao, L., et al. 2017, PASA, 34, e058
   Groves, B., Krause, O., Sandstrom, K. et al. 2012, MNRAS, 426, 892
   Mathis, J.S., Mezger, P.G., & Panagia, N. 1983, A&A, 128, 212
   Stanway, E.R., & Eldridge, J.J. 2018, MNRAS, 479, 75



===========================

Description of files:

1. Emission Spectra for Individual Grain sizes:
Filenames:
   iout_dusttype_startype_lgU.gz
   iout_dusttype_startype_lgU_slab.gz

where dusttype =
   graD16emtPAHib for PAH ions
   graD16emtPAHnb for PAH neutrals
   DH21Ad_P0.20_0.00 for DH21 Astrodust, P=0.20, f_Fe=0

startype =
   mmpisrf : mMMP spectrum (see Draine, Li, Hensley et al 2021)
   bc03_z0.02_3e6 : BC03 3 Myr old stellar population with Z=0.02
   bc03_z0.0004_1e7 : BC03 10 Myr old stellar population with Z=0.0004
   bc03_z0.02_1e7 : BC03 10 Myr old stellar population with Z=0.02
   bc03_z0.02_1e8 : BC03 100 Myr old stellar population with Z=0.02
   bc03_z0.02_3e8 : BC03 300 Myr old stellar population with Z=0.02
   bc03_z0.02_1e9 : BC03 1 Gyr old stellar population with Z=0.02
   bpass_z0.02_3e6 : BPASS 3 Myr old stellar population with Z=0.02
   bpass_z0.001_1e7 : BPASS 10 Myr old stellar population with Z=0.001
   bpass_z0.02_1e7 : BPASS 10 Myr old stellar population with Z=0.02
   bpass_z0.02_1e8 : BPASS 100 Myr old stellar population with Z=0.02
   bpass_z0.02_3e8 : BPASS 300 Myr old stellar population with Z=0.02
   bpass_z0.02_1e9 : BPASS 1 Gyr old stellar population with Z=0.02
   m31blge : M31 bulge spectrum

lgU = log_10(U) = 0.00 0.50 1.00 1.50 2.00 2.50 3.00 3.50 4.00 4.50 5.00 5.50 6.00 6.50 7.00

file structure for N = 1 - 167 [a = 3.548e-8 cm to 5.012e-4 cm] in 40 steps per decade in particle radius:
   lines 1-3 : comments
   line 4 + (N-1)*2511 : description of starlight spectrum
   line 5: (N-1)*2511 :blank
   line 6 + (N-1)*2511 : a(cm) = 10.**(-7.45+0.025*(N-1))
   lines 7-14 + (N-1)*2511 : comments
   lines 15-2514 + (N-1)*2511:
      column 1: lambda (um)
      column 2: nu * P_nu (erg s-1) [P_nu = time-averaged power per unit frequency for one particle]
      column 3: C_abs (cm2) [absorption cross section for one particle]

2. Emission Spectra Summed over Simple Dust Model for a Single U, for Dust Abundance per H Appropriate for the Local Diffuse ISM:
Filenames:
   pahspec.out_startype_lgU_fion_fsize.gz
   pahspec.out_startype_lgU_slab_fion_fsize.gz

where
   startype is as above
   fion = lo, st, hi for low, standard, high f_ion
   fsize = sma, std, lrg for small, standard, large size distribution

file structure:
   line 1: description of radiation field
   line 2: TIR power radiated by Astrodust (erg s-1 per H)
   line 3: TIR power radiated by PAH^+ (erg s-1 per H)
   line 4: TIR power radiated by PAH^0 (erg s-1 per H)
   line 5: total TIR power radiated (erg s-1 per H)
   lines 6-7: comments
   lines 8-1980: (1973 wavelengths from 1.0056 um to 8000um)
      column 1: wave (um)
      column 2: nu*P_nu (total) (erg s-1 per H) [P_nu=power radiated per unit frquency per H]
      column 3: nu*P_nu (Astrodust only) (erg s-1 per H)
      column 4: nu*P_nu (PAH^+ only) (erg s-1 per H)
      column 5: nu*P_nu (PAH^- only) (erg s-1 per H)

3. PAH Ionization and Size Distributions Used in Computing pahspec.out:
Filenames:
   pahspec_dnda.out_fion_fsize

where
   fion = lo, st, hi for low, standard, or high PAH ionization
   fsize = sma, std, lrg for small, standard, large PAH size distribution

file structure:
   lines 1-3: comments
   lines 4-170:
      column 1: a_eff (um)
      column 2: dn_Ad = astrodust number per H in this bin
      column 3: dn_PAH+ = PAH^+ number per H in this bin
      column 4: dn_PAH0 = PAH^0 number per H in this bin

4. Unreddened Starlight Spectra Used in Calculations for U = 1 :
Filenames:
   isrf_startype_0.00.gz

where
   startype is as above

file structure:
   line 1: comments
   line 2: U
   line 3: u = u_starlight+u_CMB = photon energy density (erg cm-3)
   line 4: u_starlight = starlight energy density (erg cm-3)
   line 5: < h nu > = mean photon energy (eV) [including CMB photons]
   line 6-7: comments
   lines 8-2507:
      column 1: lambda (um)
      column 2: lambda*u_lambda (erg cm-3)

